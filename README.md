# NutscraftState

一款实时获取 Minecraft 服务器在线信息的网站程序  
方便玩家查看服务器信息  
基于php实现，API接口采用 [洛神云](https://mc.iroselle.com/) 开放API  

## 预览

[点我预览](https://mc.n0ts.cn/state/)

![](https://s1.ax1x.com/2020/10/16/0HKIDs.png)

## 使用方法

`index.php` 中修改 16 行为自己服务器ip即可