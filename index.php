<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
    <title>Nutscraft服务器状态</title>
    <link rel="stylesheet" href="./css/minecraft_font.css">
    <link rel="stylesheet" href="./css/ServerInfo.css">
</head>

<body>
    <!-- 获取服务器状态信息 -->
    <?php
        // 服务器ip
        $Server_IP = "mc.nutssss.top";
        $Server_Info = file_get_contents("https://mc.iroselle.com/api/data/getServerInfo?server_addr=" . $Server_IP);
        $Server_Info = json_decode($Server_Info, true);
    ?>

    <div id="Server_Info">

        <!-- 顶部 -->
        <div class="title">
            <p>Nutscraft状态？</p>
        </div>

        <!-- 连接成功 -->
        <div class="Server_On Server_Box">
            <div class="img">
                <img src="./images/wdnmd.gif" alt="Server_icons">
            </div>
            <ul class="ul_left">
                <li>Nutscraft - 1.16.2 | 多线</li>
                <li><span style="color: rgb(85,255,85);">Nutscraft-</span>
                    <span style="color: rgb(255,85,255);">1.16.2</span>
                </li>
                <li>
                    <span style="color: rgb(85,255,255);">生存</span>
                    <span style="color: rgb(255,85,85);">-</span>
                    <span style="color: rgb(255,255,85);">空岛</span>
                    <span style="color: rgb(255,85,85);">-</span>
                    <span style="color: rgb(85,255,85);">纯净</span>
                    <span style="color: rgb(255,85,85);">-</span>
                    <span style="color: rgb(0,170,170);">公益</span>
                </li>
            </ul>
            <ul class="ul_right">
                <li>在线人数：<?php echo $Server_Info['res']['server_player_online'], "/", $Server_Info['res']['server_player_max'];?>
                </li>
                <li>昨日最高在线：<?php echo $Server_Info['res']['server_player_yesterday_max'];?></li>
                <li>历史最高在线：<?php echo $Server_Info['res']['server_player_history_max'];?></li>
            </ul>
            <div class="signal on"></div>
        </div>

        <!-- 连接失败 -->
        <div class="SerVer_Off Server_Box">
            <div class="img">
                <img src="./images/wdnmd.gif" alt="Server_icons">
            </div>
            <ul class="ul_left">
                <li>Nutscraft - 1.16.2 | 多线</li>
                <li style="color: red;">服务器连接失败！<br />请联系服主坚果进行检查！</li>
            </ul>
            <div class="signal off"></div>
        </div>

        <!-- 页脚 -->
        <div class="footer">
            <div class="buttom_box">
                <div class="buttom">
                    <a href="https://mc.n0ts.cn" target="_blank">前往官网</a>
                </div>
                <div class="buttom">
                    <a href="https://mc.n0ts.cn/wiki" target="_blank">前往Wiki</a>
                </div>
                <div class="buttom">
                    <a href="https://mc.n0ts.cn/wiki/?p=68" target="_blank">加入我们</a>
                </div>
            </div>
        </div>
    </div>

    <!-- js -->
    <script type="text/javascript">
        if ("<?php echo $Server_Info['res']['server_status'];?>" == 1) {
            // alert("服务器连接成功");
            var Server_On = document.getElementsByClassName("Server_On")[0].style.display = "block";
            var SerVer_Off = document.getElementsByClassName("SerVer_Off")[0].style.display = "none";
        } else {
            // alert("服务器连接失败！请联系服主坚果进行检查！");
            var SerVer_Off = document.getElementsByClassName("SerVer_Off")[0].style.display = "block";
            var Server_On = document.getElementsByClassName("Server_On")[0].style.display = "none";
        }
    </script>
</body>

</html>